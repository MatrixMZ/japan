// MaterializeCSS
$(document).ready(function(){
    $('.sidenav').sidenav();
    $(".dropdown-trigger").dropdown({
        constrainWidth: false
    });
    $('.parallax').parallax();
    $('nav').removeClass().addClass(["transparent","z-depth-0"]);
    $('.collapsible').collapsible();
});

//Custom navbar on-scroll color changer
$(window).scroll(function(){                          
    if ($(this).scrollTop() > 10) {
        $('nav').removeClass().addClass(['bg-pattern','grey','darken-4']);
    }else{
        $('nav').removeClass().addClass(["transparent","z-depth-0"]);
    }
});

function route(link){
    location.href = link;
}
