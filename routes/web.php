<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index')->name('index-index');

Route::group(['prefix' => 'cars'], function () {
   Route::get('/', 'CarsController@index')->name('cars-index');
   Route::get('{option}', 'CarsController@group')->name('cars-group');
   Route::get('show/{id}', 'CarsController@show')->name('cars-show');
});

Route::group(['prefix' => 'orders'], function () {
   Route::get('/', 'OrdersController@index')->name('orders-index');
   Route::post('/', 'OrdersController@store')->name('orders-store');
});

Route::get('/contact', 'ContactController@index')->name('contact-index');

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
