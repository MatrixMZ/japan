<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;

class OrdersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.orders.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Order::create([
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'message' => $request->message
        ]);


        $to = "szonpietrusza@gmail.com";
        $subject = "Tokyo Cars";
        $txt = "{$request->name}"."\r\n"."{$request->email}"."\r\n"."{$request->phone}"."\r\n"."\r\n"."{$request->message}";
        $headers = "From: szonpietrusza@tokyocars.pl";

        mail($to,$subject,$txt,$headers);

        if($request->email != null){
            $to = $request->email;
            $subject = "Tokyo Cars";
            $txt = "Twoja wiadomość została dostarczona, w odpowiedzi będziemy się z Tobą kontaktować w najbliższym czasie."."\r\n"."Pozdrawiamy, Tokyo Cars.";
            $headers = "From: szonpietrusza@tokyocars.pl";

            mail($to,$subject,$txt,$headers);
        }


        return view('pages.orders.confirm');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
