<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Car;
use App\Gearbox;
use App\Fueltype;
use App\Drive;
use App\Status;
use App\CarEquipment;
use App\Equipment;
use App\Shortgearbox;


class CarsController extends Controller
{
     /**
     * Prepare Model data with relations.
     *
     * @param  string  $column
     * @param  any     $data 
     * @return json 
     */
    private function getCars($column, $data){
        if(isset($column) && isset($data)){
            if($column == "status"){
                if($data == "current"){
                    $cars = Car::where('status', 1)->orderBy('created_at', 'DESC')->get();
                }else if($data == "sold"){
                    $cars = Car::where('status', 2)->get();
                }else if($data == "soon"){
                    $cars = Car::where('status', 3)->get();
                }else{
                    $cars = Car::get();
                }
            }else if($column == "id"){
                $cars = Car::where($column, $data)->get();
            }   
        }else{
            dd("There's no required parameters!");
        }
        
        //RELATIONSHIPS CONNECTOR -- Becouse Voyager's stupidity is unpredictable (I cannot get Model with Relations)
        foreach($cars as $car){
            //gearbox
            $car->gearbox = Gearbox::where('id', $car->gearbox)->first()->name;
            //gearbox shortcut
            $car->gearbox_shortcut = Shortgearbox::where('id', $car->gearbox_shortcut)->first()->name;
            //fueltype
            $car->fueltype = Fueltype::where('id', $car->fueltype)->first()->name;
            //drive
            $car->drive = Drive::where('id', $car->drive)->first()->name;
            //status
            $car->status = Status::where('id', $car->status)->first()->name;
            //photos
            // $car->photos = json_decode($car->photos);

            //EQUIPMENT...
            $pivots = CarEquipment::where('car_id', $car->id)->get();
            $array = array();
            foreach($pivots as $pivot){
                array_push($array, Equipment::where('id', $pivot->equipment_id)->first()->name);
            }
            $car->equipment = $array;

            //main image
            $car->image = json_decode($car->photos, true)[0];
        }
        if($column == "status"){
            return $cars;
        }else if($column == "id"){
            return $cars->first();
        }
            
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.cars.index');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function group($option)
    {
        $cars = $this->getCars('status' ,$option);
        $status = $option;
        return view('pages.cars.group', compact('cars', 'status'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $car = $this->getCars('id', $id);
        // dd($car);
        return view('pages.cars.show', compact(['car']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
