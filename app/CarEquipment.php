<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class CarEquipment extends Model
{
    public $table = 'car_equipment';
}
