@extends('layout.master')

@section('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/paralax.css') }}">
@endsection

@section('title')
  {!! setting('site.title') !!}
@endsection

@section('content')

<div class="parallax-container center valign-wrapper minus-nav">
    <div id='stars'></div>
    <div id='stars2'></div>
    <div id='stars3'></div>
    <div class="container">
        <div class="row">
          <div class="col s12 white-text">
            <h1 class="text-darken-3">{!! setting('strona-glowna.home_title') !!}</h1>
            <p class="flow-text" id="typed"></p>
            <a class="waves-effect waves-light btn-large cyan darken-3 hoverable thin-font" href="{{ route('cars-index') }}">{{setting('strona-glowna.home_button')}}</a>
          </div>
        </div>
    </div>

    <div class="parallax">
        <img src="{{ asset('storage/'.setting('strona-glowna.background_image')) }}">
    </div>

</div>

<div class="row">
    {{-- Lines with icon --}}
    <div class="space__more"></div>
    <div class="space__more"></div>
    <div class="col s12"><div class="hr-sect"><i class="material-icons medium">graphic_eq</i></div></div>


    {{-- Card with links --}}
    <div class="col s12 m10 offset-m1 thin-font">
        <div class="space__more"></div>
        <div class="space__more"></div>
        <div class="row">
            {{-- Current --}}
            <div class="col s12 m4">
                <div class="card blue-grey darken-1 pointer" onclick="route('{{ route('cars-group', ['option' => 'current']) }}')">
                    <div class="card-content white-text">
                      <span class="card-title">{!!setting('strona-glowna.current_title')!!}</span>
                      <p>{!! setting('strona-glowna.current_description') !!}</p>
                    </div>
                    <div class="card-action">
                        <a href="{{ route('cars-group', ['option' => 'current']) }}"><i class="material-icons left">link</i>Przejdź dalej</a>
                    </div>
                </div>
            </div>
            {{-- Soon --}}
            <div class="col s12 m4">
                <div class="card blue-grey darken-1 pointer" onclick="route('{{ route('cars-group', ['option' => 'current']) }}')">
                    <div class="card-content white-text">
                      <span class="card-title">{!! setting('strona-glowna.soon_title') !!}</span>
                      <p>{!! setting('strona-glowna.soon_description') !!}</p>
                    </div>
                    <div class="card-action">
                        <a href="{{ route('cars-group', ['option' => 'soon']) }}"><i class="material-icons left">link</i>Przejdź dalej</a>
                    </div>
                </div>
            </div>
            {{-- Sold --}}
            <div class="col s12 m4">
                <div class="card blue-grey darken-1 pointer" onclick="route('{{ route('cars-group', ['option' => 'current']) }}')">
                    <div class="card-content white-text">
                      <span class="card-title">{!! setting('strona-glowna.sold_title') !!}</span>
                      <p>{!! setting('strona-glowna.sold_description') !!}</p>
                    </div>
                    <div class="card-action">
                        <a href="{{ route('cars-group', ['option' => 'sold']) }}"><i class="material-icons left">link</i>Przejdź dalej</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="space__more"></div>
    </div>

    {{-- Informations --}}
    <div class="col s12 m10 offset-m1">
        <h4 class="white-text">
            <i class="material-icons small left thin-font">info</i>{!! setting('strona-glowna.info_title') !!}
        </h4>
        <p class="white-text flow-text thin-font">{!! setting('strona-glowna.info_description') !!}</p>
        <div class="space__more"></div>
        <div class="space__more"></div>
    </div>
</div>
@endsection

@section('scripts')
    <script type="text/javascript" src="{{ asset('typed/typed.min.js') }}"></script>
    <script type="text/javascript">
        
        $(document).ready(function(){
            var options = {
                strings: ["{{setting('strona-glowna.home_description')}}"],
                typeSpeed: 50,
                showCursor: false
            }
            var typed = new Typed("#typed", options);
        });
    </script>
@endsection