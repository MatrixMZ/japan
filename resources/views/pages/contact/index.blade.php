@extends('layout.master')

@section('title')
    Kontakt
@endsection

@section('css')
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.1/dist/leaflet.css" integrity="sha512-Rksm5RenBEKSKFjgI3a41vrjkw4EVPlJ3+OiI65vTjIdo9brlAacEuKOiQ5OFh7cOI1bkDwLqdLw3Zg0cRJAAQ==" crossorigin=""/>
@endsection


@section('content')
{{-- Info Nav --}}
<div class="row minus-nav grey darken-4 minus-nav__spacer z-depth-3 thin-font">
    <div class="col s12 m10 offset-m1 info-nav flow-text white-text">
        <p>
            <div class="hr-sect"><i class="material-icons medium">account_box</i></div>
        </p>
        <p class="center-align">
            {!! setting('kontakt.contact_description') !!}
        </p>
        <p class="center-align make-inline">
            {!! '<span><i class="material-icons">contact_phone</i>&nbsp;&nbsp;'.setting('kontakt.contact_phone')."</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" !!}
            {!! '<span><i class="material-icons">contact_mail</i>&nbsp;&nbsp;'.setting('kontakt.contact_email').'</span>' !!}
        </p>
    </div>
</div>
{{-- Leaflet map --}}
<div id="mapid" style="height: 500px; margin: -15px 0" class="row"></div>
@endsection

@section('scripts')
    <script src="https://unpkg.com/leaflet@1.3.1/dist/leaflet.js"
   integrity="sha512-/Nsx9X4HebavoBvEBuyp3I7od5tA0UzAxs+j83KgC8PU0kgB4XiK4Lfe4y4cgBtaRJQEIFCW+oC506aPT2L1zw=="
   crossorigin=""></script>
   <script type="text/javascript">
        var mymap = L.map('mapid', { scrollWheelZoom: false }).setView([{!! setting('kontakt.coords_x') !!}, {!! setting('kontakt.coords_y') !!}], {!! setting('kontakt.zoom') !!});

        L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
            attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
            maxZoom: 18,
            id: 'mapbox.streets',
            accessToken: 'pk.eyJ1IjoibWF0cml4bXoiLCJhIjoiY2ppb2N3eDA0MDhkdjNwdDlqY2IxeGdpYyJ9.wf3bhipjdcFJqjN-zSwtCQ'
        }).addTo(mymap);

        var marker = L.marker([{!! setting('kontakt.coords_x') !!}, {!! setting('kontakt.coords_y') !!}]).addTo(mymap);

        marker.bindPopup("{!! setting('kontakt.map_description') !!}").openPopup();
   </script>

@endsection