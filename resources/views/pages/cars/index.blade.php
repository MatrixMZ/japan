@extends('layout.master')

@section('title')
    Samochody
@endsection

@section('content')
{{-- Info Nav --}}
<div class="row minus-nav grey darken-4 z-depth-3">
    <div class="col s12 m10 offset-m1 info-nav flow-text white-text">
        <span class="thin-font">
            <p>
                <a href="{{ route('index-index') }}" class="breadcrumb amber-text">Strona Główna</a>
                <a class="breadcrumb amber-text">Samochody</a>
            </p>
            
            {!! '<p>'.setting('samochody.main_description').'</p>' !!}
            
        </span>
    </div>
</div>

{{-- Card with links --}}
<div class="row">
    <div class="col s12 m10 offset-m1 thin-font">
        <div class="space__more"></div>
        <div class="space__more"></div>
        <div class="row">
            {{-- Current --}}
            <div class="col s12 m4">
                <div class="card blue-grey darken-1 pointer" onclick="route('{{ route('cars-group', ['option' => 'current']) }}')">
                    <div class="card-content white-text">
                      <span class="card-title">{!!setting('strona-glowna.current_title')!!}</span>
                      <p>{!! setting('strona-glowna.current_description') !!}</p>
                    </div>
                    <div class="card-action">
                        <a href="{{ route('cars-group', ['option' => 'current']) }}"><i class="material-icons left">link</i>Przejdź dalej</a>
                    </div>
                </div>
            </div>
            {{-- Soon --}}
            <div class="col s12 m4">
                <div class="card blue-grey darken-1 pointer" onclick="route('{{ route('cars-group', ['option' => 'soon']) }}')">
                    <div class="card-content white-text">
                      <span class="card-title">{!! setting('strona-glowna.soon_title') !!}</span>
                      <p>{!! setting('strona-glowna.soon_description') !!}</p>
                    </div>
                    <div class="card-action">
                        <a href="{{ route('cars-group', ['option' => 'soon']) }}"><i class="material-icons left">link</i>Przejdź dalej</a>
                    </div>
                </div>
            </div>
            {{-- Sold --}}
            <div class="col s12 m4">
                <div class="card blue-grey darken-1 pointer" onclick="route('{{ route('cars-group', ['option' => 'sold']) }}')">
                    <div class="card-content white-text">
                      <span class="card-title">{!! setting('strona-glowna.sold_title') !!}</span>
                      <p>{!! setting('strona-glowna.sold_description') !!}</p>
                    </div>
                    <div class="card-action">
                        <a href="{{ route('cars-group', ['option' => 'sold']) }}"><i class="material-icons left">link</i>Przejdź dalej</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="space__more"></div>
    </div>
</div>

@endsection