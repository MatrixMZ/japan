@extends('layout.master')

@section('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('lightbox2/css/lightbox.css') }}">
@endsection

@section('title')
    {{$car->marka}} {{$car->model}}
@endsection

@section('content')
{{-- Informations --}}
<div class="row minus-nav grey darken-4 minus-nav__spacer z-depth-3">
    {{-- Back Button --}}
    <div class="col s12 m10 offset-m1 info-nav flow-text white-text">
        <a href="{{ URL::previous() }}" class="btn cyan darken-3 thin-font">
            <i class="material-icons white-text left">reply</i> Powrót
        </a>
    </div>

    {{-- Car Card --}}
    <div class="col s12 m3 offset-m1">
        <div onclick="openGallery()" class="z-depth-1 blue-grey darken-4 white-text hoverable pointer fixed-card waves-effect">
            {{-- Card - Image --}}
            <div class="card-header white">
                <div class="card-padding-box waves-effect waves-light" style="background-image: url('{{ Voyager::image($car->thumbnail('medium')) }}')">
                    <i class="photo-box-icon material-icons medium">zoom_in</i>
                </div>
            </div>
            {{-- Card - Name & Price --}}
            <div class="card-body">
                <h2 class="name thin-font">{{$car->marka}} {{$car->model}}</h2>
                <h4 class="price amber-text thin-font">
                    @if($car->status !== 'Sprzedane')
                    {{number_format($car->price, 0, '.', ' ')." PLN"}}
                    @else
                        SPRZEDANE
                    @endif
                </h4>
            </div>
            {{-- Card - Parameters --}}
            <div class="card-footer">
                <div class="row">
                    <div class="stats col s12">
                        <div class="stat col s6">
                            <span class="label grey-text text-lighten-3">Pojemność</span>
                            <span class="value">{{number_format($car->capacity, 0, '.', ' ')}} cm<sup>3</sup></span>
                        </div>
                        <div class="stat col s6 left-border">
                            <span class="label grey-text text-lighten-3">Przebieg</span>
                            <span class="value">{{number_format($car->mileage, 0, '.', ' ')." km"}}</span>
                        </div>
                    <div class="col s12 space"></div>
                    <div class="col s12 space"></div>
                        <div class="stat col s4">
                            <span class="label grey-text text-lighten-3">Moc</span>
                            <span class="value">{{$car->power." KM"}}</span>
                        </div>
                        <div class="stat col s4 left-border">
                            <span class="label grey-text text-lighten-3">Rocznik</span>
                            <span class="value">{{$car->productionyear}}</span>
                        </div>
                        <div class="stat col s4 left-border">
                            <span class="label grey-text text-lighten-3">Skrzynia</span>
                            <span class="value">{{$car->gearbox_shortcut}}</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    {{-- Parameters --}}
    <div class="col s12 m7 thin-font boxed">
        <div class="row">
            {{-- <div class="header white-text">
                <i class="material-icons amber-text left">label</i><b>Specyfikacja</b>
            </div> --}}
            <div class="col s12 white-text parameters">
                <table>
                    <tbody>
                        <tr>
                            <td>Cena:</td>
                            <td class="amber-text"><b>{{number_format($car->price, 0, '.', ' ')." PLN"}}</b></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Marka:</td>
                            <td class="white-text"><b>{{$car->marka}}</b></td>
                            <td>Model:</td>
                            <td class="white-text"><b>{{$car->model}}</b></td>
                        </tr>
                        <tr>
                            <td>Skrzynia biegów:</td>
                            <td class="white-text"><b>{{$car->gearbox}}</b></td>
                            <td>Pojemość silnika:</td>
                            <td class="white-text"><b>{{number_format($car->capacity, 0, '.', ' ')}}cm<sup>3</sup></b></td>
                        </tr>
                        <tr>
                            <td>Przebieg:</td>
                            <td class="white-text"><b>{{number_format($car->mileage, 0, '.', ' ')." km"}}</b></td>
                            <td>Paliwo:</td>
                            <td class="white-text"><b>{{$car->fueltype}}</b></td>
                        </tr>
                        <tr>
                            <td>Moc:</td>
                            <td class="white-text"><b>{{$car->power}} KM</b></td>
                            <td>Kolor:</td>
                            <td class="white-text"><b>{{$car->color}}</b></td>
                        </tr>
                        <tr class="no-border">
                            <td>Napęd:</td>
                            <td class="white-text"><b>{{$car->drive}}</b></td>
                            <td>Rok produkcji:</td>
                            <td class="white-text"><b>{{$car->productionyear}}</b></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="space__more col s12"></div>
    
    {{-- Equipment --}}
    <div class="col s12 m10 offset-m1 thin-font blue-grey darken-4 z-depth-1 boxed">
        
        <div class="row">
            <div class="header white-text">
                <i class="material-icons amber-text left">label</i><b>Wyposażenie</b>
            </div>
            @foreach ($car->equipment as $equipment)
                <div class="col s12 m4 item">
                    <i class="material-icons left amber-text">chevron_right</i>
                    {{$equipment}}
                </div>
            @endforeach
        </div>
    </div>
    <div class="space__more col s12"></div>
    {{-- Description --}}
    <div class="col s12 m10 offset-m1 thin-font blue-grey darken-4 z-depth-1 boxed">
        <div class="row">
            <div class="header white-text">
                <i class="material-icons amber-text left">label</i><b>Informacje o samochodzie</b>
            </div>
            <div class="col s12 white-text description">{!! $car->description !!}</div>
           
        </div>
    </div>
    <div class="space__more col s12"></div>
    <div class="space__more col s12"></div>
</div>


{{-- Photo Gallery --}}
<div class="row">
    <div class="col s12">
        @for ($i = 0; $i < count(json_decode($car->photos)); $i++)
           <div class="col s6 m3">
                <a 
                @if ($i == 0)
                    id="gallery"
                @endif
                class="photo-box z-depth-1 hoverable waves-effect waves-light" 
                href="{{Voyager::image(json_decode($car->photos)[$i])}}" data-lightbox="roadtrip" 
                style="background-image: url('{{Voyager::image(json_decode($car->thumbnail('medium', 'photos'))[$i])}}');"
                >
                    <i class="material-icons medium photo-box-icon">zoom_in</i>
                </a>
            </div>
        @endfor
    </div>
</div>
@endsection

@section('scripts')
    <script type="text/javascript" src="{{ asset('lightbox2/js/lightbox.min.js') }}"></script>
    <script>
        lightbox.option({
          'resizeDuration': 200,
          // 'disableScrolling': true,
          'imageFadeDuration': 700,
          // 'wrapAround': true
        })
        function openGallery(){
            document.getElementById("gallery").click();
        }
    </script>
@endsection