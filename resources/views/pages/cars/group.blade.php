@extends('layout.master')

@section('title')
    Samochody{!!' - '.$status!!}
@endsection

@section('content')
{{-- Info Nav --}}
<div class="row minus-nav grey darken-4 z-depth-3">
    <div class="col s12 m10 offset-m1 info-nav flow-text white-text">
        <span class="thin-font">
            <p>
                <a href="{{ route('index-index') }}" class="breadcrumb amber-text">Strona Główna</a>
                <a href="{{ route('cars-index') }}" class="breadcrumb amber-text">Samochody</a>
                <a class="breadcrumb amber-text">
                    @if($status == "current")
                        Aktualne
                    @elseif($status == "soon")
                        Wkrótce
                    @elseif($status == "sold")
                        Sprzedane
                    @endif
                </a>
                
            </p>
            <i class="material-icons white-text">info</i> 
            @if (count($cars))
                @if($cars[0]->status == "Aktualne")
                    {!! setting('samochody.current') !!}
                @elseif($cars[0]->status == "Wkrótce")
                    {!! setting('samochody.soon') !!}
                @elseif($cars[0]->status == "Sprzedane")
                    {!! setting('samochody.sold') !!}
                @endif
            @else
                {!! setting('samochody.no_cars') !!}
            @endif
        </span>
    </div>
</div>

<div class="row">
    <div class="col s12 m10 offset-m1">
        <div class="row">
            <div class="col s12">
                @foreach($cars as $car)
                <div class="col s12 m6 l4">
                    <div class="z-depth-1 blue-grey darken-4 white-text hoverable pointer fixed-card waves-effect" onclick="route('{{ route('cars-show', ['id' => $car->id]) }}')">

                        <div class="card-header white">
                            <div class="card-padding-box waves-effect waves-light" style="background-image: url('{{ Voyager::image($car->thumbnail('medium')) }}')">
                                <i class="photo-box-icon material-icons medium">send</i>
                            </div>
                        </div>

                        <div class="card-body">
                            <h2 class="name thin-font">{{$car->marka}} {{$car->model}}</h2>
                            <h4 class="price amber-text thin-font">
                                @if($status !== 'sold')
                                {{number_format($car->price, 0, '.', ' ')." PLN"}}
                                @else
                                    SPRZEDANE
                                @endif
                            </h4>
                        </div>
                        <div class="card-footer">
                            <div class="row">
                                <div class="stats col s12">
                                    <div class="stat col s6">
                                        <span class="label grey-text text-lighten-3">Pojemność</span>
                                        <span class="value">{{number_format($car->capacity, 0, '.', ' ')}} cm<sup>3</sup></span>
                                    </div>
                                    <div class="stat col s6 left-border">
                                        <span class="label grey-text text-lighten-3">Przebieg</span>
                                        <span class="value">{{number_format($car->mileage, 0, '.', ' ')." km"}}</span>
                                    </div>
                                
                                    <div class="col s12 space"></div>
                                    <div class="col s12 space"></div>
                                
                                    <div class="stat col s4">
                                        <span class="label grey-text text-lighten-3">Moc</span>
                                        <span class="value">{{$car->power." KM"}}</span>
                                    </div>
                                    <div class="stat col s4 left-border">
                                        <span class="label grey-text text-lighten-3">Rocznik</span>
                                        <span class="value">{{$car->productionyear}}</span>
                                    </div>
                                    <div class="stat col s4 left-border">
                                        <span class="label grey-text text-lighten-3">Skrzynia</span>
                                        <span class="value">{{$car->gearbox_shortcut}}</span>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                @endforeach
            </div>
            <h4>&nbsp;</h4>
        </div>
    </div>
</div>

@endsection