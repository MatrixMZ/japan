@extends('layout.master')

@section('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('lightbox2/css/lightbox.css') }}">
@endsection

@section('title')
    {{$car->marka}} {{$car->model}}
@endsection

@section('content')
{{-- Informations --}}
<div class="row minus-nav grey darken-4 minus-nav__spacer z-depth-3">
    {{-- Back Button --}}
    <div class="col s12 m10 offset-m1 info-nav flow-text white-text">
        <a href="{{ URL::previous() }}" class="btn cyan darken-3 thin-font">
            <i class="material-icons white-text left">reply</i> Powrót
        </a>
    </div>

    {{-- Car Card --}}
    <div class="col s12 m3 offset-m1">
        <div onclick="openGallery()" class="z-depth-1 blue-grey darken-4 white-text hoverable pointer fixed-card waves-effect">
            {{-- Card - Image --}}
            <div class="card-header white">
                <div class="card-padding-box waves-effect waves-light" style="background-image: url('{{ Voyager::image($car->thumbnail('medium')) }}')">
                    <i class="photo-box-icon material-icons medium">zoom_in</i>
                </div>
            </div>
            {{-- Card - Name & Price --}}
            <div class="card-body">
                <h2 class="name thin-font">{{$car->marka}} {{$car->model}}</h2>
                <h4 class="price amber-text thin-font">
                    @if($car->status !== 'Sprzedane')
                    {{number_format($car->price, 0, '.', ' ')." PLN"}}
                    @else
                        SPRZEDANE
                    @endif
                </h4>
            </div>
            {{-- Card - Parameters --}}
            <div class="card-footer">
                <div class="row">
                    <div class="stats col s12">
                        <div class="stat col s6">
                            <span class="label grey-text text-lighten-3">Pojemność</span>
                            <span class="value">{{number_format($car->capacity, 0, '.', ' ')}} cm<sup>3</sup></span>
                        </div>
                        <div class="stat col s6 left-border">
                            <span class="label grey-text text-lighten-3">Przebieg</span>
                            <span class="value">{{number_format($car->mileage, 0, '.', ' ')." km"}}</span>
                        </div>
                    <div class="col s12 space"></div>
                    <div class="col s12 space"></div>
                        <div class="stat col s4">
                            <span class="label grey-text text-lighten-3">Moc</span>
                            <span class="value">{{$car->power." KM"}}</span>
                        </div>
                        <div class="stat col s4 left-border">
                            <span class="label grey-text text-lighten-3">Rocznik</span>
                            <span class="value">{{$car->productionyear}}</span>
                        </div>
                        <div class="stat col s4 left-border">
                            <span class="label grey-text text-lighten-3">Skrzynia</span>
                            <span class="value">{{$car->gearbox_shortcut}}</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    {{-- Collapsible - Informations --}}
    <div class="col s12 m7 thin-font">
        <ul class="collapsible collapsible-fix popout m3 offset-m1 white-text">
            {{-- Collapsible - Parameters --}}
            <li>
                <div class="collapsible-header blue-grey darken-4"><i class="material-icons amber-text">label</i>Parametry</div>
                <div class="collapsible-body blue-grey darken-4">
                    <table>
                        <tbody>
                            <tr>
                                <td class="l-gray">Cena:</td>
                                <td>{{number_format($car->price, 0, '.', ' ')." PLN"}}</td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td class="l-gray">Marka:</td>
                                <td>{{$car->marka}}</td>
                                <td class="l-gray">Model:</td>
                                <td>{{$car->model}}</td>
                            </tr>
                            <tr>
                                <td class="l-gray">Skrzynia biegów:</td>
                                <td>{{$car->gearbox}}</td>
                                <td class="l-gray">Pojemość silnika:</td>
                                <td>{{number_format($car->capacity, 0, '.', ' ')}}cm<sup>3</sup></td>
                            </tr>
                            <tr>
                                <td class="l-gray">Przebieg:</td>
                                <td>{{number_format($car->mileage, 0, '.', ' ')." km"}}</td>
                                <td class="l-gray">Paliwo:</td>
                                <td>{{$car->fueltype}}</td>
                            </tr>
                            <tr>
                                <td class="l-gray">Moc:</td>
                                <td>{{$car->power}} KM</td>
                                <td class="l-gray">Kolor:</td>
                                <td>{{$car->color}}</td>
                            </tr>
                            <tr class="no-border">
                                <td class="l-gray">Napęd:</td>
                                <td>{{$car->drive}}</td>
                                <td class="l-gray">Rok produkcji:</td>
                                <td>{{$car->productionyear}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </li>
            {{-- Collapsible - Specifications --}}
            <li class="active">
                <div class="collapsible-header blue-grey darken-4"><i class="material-icons amber-text">whatshot</i>Wyposarzenie</div>
                <div class="collapsible-body blue-grey darken-4">
                    <div class="row">
                        @foreach ($car->equipment as $equipment)
                            <div class="col s12 m6 equipment"><i class="material-icons left">chevron_right</i>{{$equipment}}
                            </div>
                        @endforeach
                    </div>
                </div>
            </li>
            {{-- Collapsible - Description --}}
            <li>
                <div class="collapsible-header blue-grey darken-4"><i class="material-icons amber-text">format_quote</i>Opis</div>
                <div class="collapsible-body blue-grey darken-4">
                    {!! $car->description !!}
                </div>
            </li>
        </ul>
    </div>
</div>

{{-- Photo Gallery --}}
<div class="row">
    <div class="col s12">
        @for ($i = 0; $i < count(json_decode($car->photos)); $i++)
           <div class="col s6 m3">
                <a 
                @if ($i == 0)
                    id="gallery"
                @endif
                class="photo-box z-depth-1 hoverable waves-effect waves-light" 
                href="{{Voyager::image(json_decode($car->photos)[$i])}}" data-lightbox="roadtrip" 
                style="background-image: url('{{Voyager::image(json_decode($car->thumbnail('medium', 'photos'))[$i])}}');"
                >
                    <i class="material-icons medium photo-box-icon">zoom_in</i>
                </a>
            </div>
        @endfor
    </div>
</div>
@endsection

@section('scripts')
    <script type="text/javascript" src="{{ asset('lightbox2/js/lightbox.min.js') }}"></script>
    <script>
        lightbox.option({
          'resizeDuration': 200,
          // 'disableScrolling': true,
          'imageFadeDuration': 700,
          // 'wrapAround': true
        })
        function openGallery(){
            document.getElementById("gallery").click();
        }
    </script>
@endsection