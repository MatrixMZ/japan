@extends('layout.master')

@section('title')
    Wiadomość została wysłana!
@endsection

@section('content')
<div class="row minus-nav grey darken-4 minus-nav__spacer z-depth-3">
    <div class="col s12 m10 offset-m1 info-nav flow-text white-text">
        <span class="thin-font">
            {!! setting('zamowienia.order_confirmation') !!}
        </span>
        <p>
            <a href="{{ route('index-index') }}" class="btn cyan darken-3">
                <i class="material-icons left">reply</i>
                Powrót do strony głównej
            </a>
        </p>
    </div>
</div>
@endsection