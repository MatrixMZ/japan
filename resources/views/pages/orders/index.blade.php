@extends('layout.master')

@section('title')
    Zamów samochód
@endsection

@section('content')
    <div class="row minus-nav grey darken-4 minus-nav__spacer z-depth-3">
        <div class="col s12 m10 offset-m1 info-nav flow-text white-text">
            <span class="thin-font">
                {!! setting('zamowienia.form_description') !!}
            </span>

            {{-- Orders form --}}
            <form method="POST" action="{{ route('orders-store') }}" onsubmit="return validateForm(this)"> 
                {{csrf_field()}}

                {{-- Name & Surname --}}
                <div class="row">
                    <div class="input-field col s12 m6">
                        <i class="material-icons prefix">account_circle</i>
                        <input id="name" name="name" type="text" class="validate white-text" required>
                        <label for="name">Imie i nazwisko</label>
                    </div>
                </div>

                {{-- Email & Phone --}}
                <div class="row">
                    <div class="input-field col s12 m6">
                        <i class="material-icons prefix">mail</i>
                        <input id="email" name="email" type="email" class="validate white-text">
                        <label for="email">Adres email</label>
                    </div>
                    <div class="input-field col s12 m6">
                        <i class="material-icons prefix">phone</i>
                        <input id="phone" name="phone" type="text" class="validate white-text">
                        <label for="phone">Numer telefonu</label>
                    </div>
                </div>

                {{-- Validate error --}}
                <div class="row thin-font hide" id="error">
                    <div class="col s12">
                        <div class="card-panel cyan darken-3">
                            <span class="white-text">Musisz uzupełnić przynajmniej jedno z podanych: <i>Adres email</i> lub <i>Numer telefonu</i>. Ponieważ te dane potrzebne nam będą aby się z Tobą skontaktować.
                        </span>
                        </div>
                    </div>
                </div>


                {{-- Message --}}
                <div class="row">
                    <div class="input-field col s12">
                      <i class="material-icons prefix">mode_edit</i>
                      <textarea id="message" name="message" class="materialize-textarea white-text" required></textarea>
                      <label for="message">Wiadomość</label>
                    </div>
                </div>
                
                {{-- Submit Button --}}
                <div class="row">
                    <div class="col s12 right-align hide-on-small-only">
                        <button type="submit" class="btn hoverable z-depth-3 amber black-text"><i class="material-icons left">send</i>Złóż zamówienie!</button>
                    </div>
                    <div class="col s12 center-align hide-on-med-and-up">
                        <button type="submit" class="btn hoverable z-depth-3 amber black-text"><i class="material-icons left">send</i>Złóż zamówienie!</button>
                    </div>
                </div>

                
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col s12 m10 offset-m1">
            <p class="flow-text thin-font white-text">{!! setting('zamowienia.contact_description') !!}</p>
            <p class="center-align">
                <a href="{{ route('contact-index') }}" class="btn amber black-text">
                    <i class="material-icons left">send</i>Kontakt
                </a>
            </p>
        </div>
    </div>
    
@endsection

@section('scripts')
    <script type="text/javascript">
        function validateForm(form){
            var email = form.email.value;
            var phone = form.phone.value;

            if(email == '' && phone == ''){
                $('#error').removeClass('hide').addClass('show');
                return false;
            }else{
                return true;
            }
            
        }
    </script>
@endsection