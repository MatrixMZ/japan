{{-- Footer --}}
<footer class="page-footer bg-pattern grey darken-4 thin-font">
    <div class="container">
        <div class="row">
            <div class="col m6 s12">
                <h5 class="white-text">
                    {{setting('footer.footer_title')}}
                </h5>
                <p class="grey-text text-lighten-4">
                    {{setting('footer.footer_description')}}
                </p>
            </div>
            <div class="col m2 s12">
                <h5 class="white-text">Menu</h5>
                <ul>
                    <li><span class="grey-text text-lighten-3" ><b>Samochody</b></span></li>
                    <li><a class="grey-text text-lighten-3" href="{{ route('cars-group', ['option' => 'current']) }}">Aktualne</a></li>
                    <li><a class="grey-text text-lighten-3" href="{{ route('cars-group', ['option' => 'soon']) }}">Wkrótce</a></li>
                    <li><a class="grey-text text-lighten-3" href="{{ route('cars-group', ['option' => 'sold']) }}">Sprzedane</a></li>
                </ul>
            </div>
            <div class="col m2 s12">
                <h5 class="white-text">&nbsp;</h5>
                <ul>
                    <li><span class="grey-text text-lighten-3" ><b>Inne</b></span></li>
                    <li><a class="grey-text text-lighten-3" href="{{ route('index-index') }}">Strona Główna</a></li>
                    <li><a class="grey-text text-lighten-3" href="{{ route('contact-index') }}">Kontakt</a></li>
                    <li><a class="grey-text text-lighten-3" href="{{ route('orders-index') }}">Zamów</a></li>
                </ul>
            </div>
            <div class="col m2 s12">
                <h5 class="white-text">&nbsp;</h5>
                <ul>
                    <li><span class="grey-text text-lighten-3" ><b>Social Media</b></span></li>
                    {!! '<li><a class="grey-text text-lighten-3" target="_blank" href="'.setting('footer.facebook').'">Facebook</a></li>' !!}
                    {!! '<li><a class="grey-text text-lighten-3" target="_blank" href="'.setting('footer.instagram').'">Instagram</a></li>' !!}
                </ul>
            </div>
        </div>
    </div>
    <div class="footer-copyright bg-pattern grey darken-4 thin-font z-depth-0">
        <div class="container">
        {!! setting('footer.footer_copyright') !!}
        <a class="grey-text text-lighten-4 right" href="https://ziobrowsky.pl" target="_blank">ZiobrowSky</a>

        <span class="right">Created by:&nbsp;</span>
        </div>
    </div>
</footer>
