{{-- Navbar --}}
<div class="navbar-fixed">
    <nav class="transparent z-depth-0">
        <div class="nav-wrapper black-text">

            <a href="{{ route('index-index') }}" class="brand-logo hide-on-small-only">&nbsp;&nbsp;&nbsp;&nbsp;{!! setting('site.title') !!}&nbsp;&nbsp;&nbsp;&nbsp;</a>
            <a href="{{ route('index-index') }}" class="brand-logo hide-on-med-and-up">{!! setting('site.title') !!}</a>
            
            <ul id="nav-mobile" class="right hide-on-med-and-down">
                {{-- Dropdown Trigger --}}
                <li><a class="dropdown-trigger" href="#" data-target="dropdown1">Samochody<i class="material-icons right">arrow_drop_down</i></a></li>
                <li><a href="{{ route('orders-index') }}">Zamów</a></li>
                <li><a href="{{ route('contact-index') }}">Kontakt</a></li>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            </ul>
            {{-- Side Menu Trigger --}}
            <a href="#" data-target="slide-out" class="sidenav-trigger right"><i class="material-icons">menu</i></a>
        </div>
    </nav>
</div>

<ul id="dropdown1" class="dropdown-content grey darken-4">
    <li>
        <a class="white-text" href="{{ route('cars-group', ['option' => 'current']) }}">Aktualne{!! '&nbsp;&nbsp;<span class="cyan darken-3 badge new right">'.count(App\Car::get()->where("status", 1)).'</span>' !!}</a>
    </li>
    <li>
        <a class="white-text" href="{{ route('cars-group', ['option' => 'soon']) }}">
            Wkrótce{!! '&nbsp;&nbsp;<span class="cyan darken-3 badge new right">'.count(App\Car::get()->where("status", 3)).'</span>' !!}</a>
    </li>
    <li>
        <a class="white-text" href="{{ route('cars-group', ['option' => 'sold']) }}">Sprzedane</a>
    </li>

</ul>

{{-- SideNav Menu --}}
<ul id="slide-out" class="sidenav blue-grey darken-3">
    <li>
        <div class="user-view center-align">
            <div class="background">
                <img width="100%" src="{{ asset('img/road.jpeg') }}">
            </div>
            <a href="#user">
                {{-- <img width="120%" class="circle" src="{{ asset('img/logotype.png') }}"> --}}
            </a>
            <a href="#name">
                <span class="white-text name"><h5>{!! setting('site.title') !!}</h5>{!! setting('kontakt.contact_phone') !!}</span>
            </a>
            <a href="#email">
                <span class="white-text email">{!! setting('kontakt.contact_email') !!}</span>
            </a>
        </div>
    </li>
    {{-- Links to Home, Contact add Order --}}
    <li>
        <a class="waves-effect white-text" href="{{ route('index-index') }}">
            <i class="material-icons white-text">home</i>STRONA GŁÓWNA
        </a>
    </li>
    <li>
        <a class="waves-effect white-text" href="{{ route('contact-index') }}">
            <i class="material-icons white-text">contacts</i>KONTAKT
        </a>
    </li>
    <li>
        <a class="waves-effect white-text" href="{{ route('orders-index') }}">
            <i class="material-icons white-text">create</i>ZAMÓW
        </a>
    </li>
    {{-- Dropdown --}}
    <li class="no-padding">
        <ul class="collapsible collapsible-accordion">
          <li>
            <a class="waves-effect waves-teal btn-flat z-depth-0 collapsible-header white-text">
                <i class="material-icons left white-text">arrow_drop_down</i>
                Samochody
            </a>
            <div class="collapsible-body grey darken-4">
              <ul>
                <li>
                    <a class="white-text waves-effect" href="{{ route('cars-group', ['option' => 'current']) }}">
                        <i class="material-icons white-text">chevron_right</i>Aktualne{!! '&nbsp;&nbsp;<span class="cyan darken-3 badge new right">'.count(App\Car::get()->where("status", 1)).'</span>' !!}
                    </a>
                </li>
                <li>
                    <a class="white-text waves-effect" href="{{ route('cars-group', ['option' => 'soon']) }}">
                        <i class="material-icons white-text">chevron_right</i>Wkrótce{!! '&nbsp;&nbsp;<span class="cyan darken-3 badge new right">'.count(App\Car::get()->where("status", 3)).'</span>' !!}
                    </a>
                </li>
                <li>
                    <a class="white-text waves-effect" href="{{ route('cars-group', ['option' => 'sold']) }}">
                        <i class="material-icons white-text">chevron_right</i>Sprzedane
                    </a>
                </li>
                
              </ul>
            </div>
          </li>
        </ul>
    </li>
</ul>
