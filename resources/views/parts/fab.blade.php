{{-- Floating Action Button --}}
@if(!isset($disableFab))
<div class="fixed-action-btn click-to-toggle">
  <a class="pulse btn-floating btn-large cyan darken-3 tooltipped" data-position="left" data-tooltip="Informacje kontaktowe" href="{{ route('contact-index') }}">
    <i class="large material-icons">call</i>
  </a>
</div>
{{-- Initializtion Script --}}
<script type="text/javascript">
    $(document).ready(function(){
        $('.fixed-action-btn').floatingActionButton();
        $('.tooltipped').tooltip();
    });
</script>
@endif
