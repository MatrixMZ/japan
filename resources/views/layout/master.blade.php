<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>@yield('title')</title>

    {!! MaterializeCSS::include_full() !!}

    {{-- Styles --}}
    <link rel="stylesheet" type="text/css" href="{{ asset('css/app.css?v='.date(DATE_ATOM)) }}">
    <script type="text/javascript" src="{{ asset('js/app.js?v='.date(DATE_ATOM)) }}"></script>

    @yield('css')
</head>
<body>
    
    <main>
        @include('parts.navbar')
        @yield('content')
    </main>
    
    @include('parts.footer')
    @include('parts.fab')
    @yield('scripts')
        <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-88426843-6"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-88426843-6');
    </script>

</body>
</html>